import pygame, pymunk, sys
import pymunk.pygame_util
from random import randint

pygame.init()

width = 1164
height = 900
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("OP Physics Simulation Tool")
draw_options = pymunk.pygame_util.DrawOptions(screen)
draw_options.DRAW_CONSTRAINTS = 0
clock = pygame.time.Clock()

space = pymunk.Space()
space.gravity = (0,600)

bg_color = (217, 217, 217)
fps = 120

randomized_size = True

walls = []

def create_walls(space, h):
    w = 1070
    rects = [
        [(w/2, h - 5), (w, 10)],
        [(w/2, 5), (w, 10)],
        [(5, h/2), (10, h)],
        [(w - 5, h/2), (70, h)],
    ]

    for pos, size in rects:
        body = pymunk.Body(body_type=pymunk.Body.STATIC)
        body.position = pos
        shape = pymunk.Poly.create_box(body, size)
        shape.elasticy = 0.3
        shape.friction = 0.3
        walls.append([body, shape])
        space.add(body, shape)

class dynamic_object():
   def __init__(self, pos, size, mass, inertia, shape, shape_type, space):
       if randomized_size:
           if shape_type == "circle" or shape_type == "triangle":
               size += randint(-size // 2, size // 2)
           if shape_type == "box":
               num1, num2 = size
               if not num1 == num2:
                   num1 += randint(-num1 // 2, num1 // 2)
                   num2 += randint(-num2 // 2, num2 // 2)
               else:
                   addition = randint(-num1 // 2, num1 // 2)
                   num1 += addition
                   num2 += addition
               size = (num1, num2)
       if shape_type == "circle" or shape_type == "triangle":
           self.body = pymunk.Body(mass, inertia, body_type = pymunk.Body.DYNAMIC)
       elif shape_type == "box":
           self.body = pymunk.Body()
       self.body.position = pos
       if shape_type == "triangle":
           x,y = pos
           self.body.position = (x + size/2, y + size/2)
           points = [
               (0, 0),
               (- size, 0),
               (- size/2,- size)
           ]
           self.shape = shape(self.body, points)
       else:
           self.shape = shape(self.body, size)
       self.body.angle = angle
       self.shape.elasticy = 0.4
       self.shape.friction = 0.4
       self.shape.mass = mass
       self.shape_type = shape_type
       self.size = size
       space.add(self.body, self.shape)

class static_object():
    def __init__(self, pos, size, shape, shape_type, space):
        if randomized_size:
            if shape_type == "circle" or shape_type == "triangle":
               size += randint(-size // 2, size // 2)
            if shape_type == "box":
               num1, num2 = size
               if not num1 == num2:
                   num1 += randint(-num1 // 2, num1 // 2)
                   num2 += randint(-num2 // 2, num2 // 2)
               else:
                   addition = randint(-num1 // 2, num1 // 2)
                   num1 += addition
                   num2 += addition
               size = (num1, num2)
        self.body = pymunk.Body(body_type = pymunk.Body.STATIC)
        self.body.position = pos
        self.body.angle = angle
        if shape_type == "triangle":
            x,y = pos
            self.body.position = (x + size/2, y + size/2)
            points = [
                (0, 0),
                (- size, 0),
                (- size/2,- size)
            ]
            self.shape = shape(self.body, points)
        else:
            self.shape = shape(self.body, size)
        self.shape_type = shape_type
        self.size = size
        space.add(self.body, self.shape)

def reset_space():
    global space, angleball
    for obj in space.bodies:
        space.remove(obj)
    for obj in space.shapes:
        space.remove(obj)
    angleball = static_object((width - 31, 40), 25, pymunk.Circle, "anglecircle", space)


shape = pymunk.Circle
shape_name = "circle"
size = 30
angle = 0
mass = 125
inertia = 30

angleball = static_object((width - 31, 40), 25, pymunk.Circle, "anglecircle", space)

def main():
    run = True
    create_walls(space, height)
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                global size, shape, shape_name, angle, angleball, mass, inertia
                if event.key == pygame.K_1:
                    shape = pymunk.Circle
                    shape_name = "circle"
                    size = 30
                    mass = 125
                    inertia = 30
                if event.key == pygame.K_2:
                    shape = pymunk.Poly.create_box
                    shape_name = "box"
                    size = (100, 60)
                    mass = 200
                    inertia = 30
                if event.key == pygame.K_3:
                    shape = pymunk.Poly
                    shape_name = "triangle"
                    size = 60
                    mass = 110
                    inertia = 20
                if event.key == pygame.K_4:
                    shape = pymunk.Poly.create_box
                    shape_name = "box"
                    size = (65, 65)
                    mass = 130
                    inertia = 25
                if event.key == pygame.K_s:
                    global randomized_size
                    if randomized_size:
                        randomized_size = False
                    else:
                        randomized_size = True
                if event.key == pygame.K_BACKSPACE or event.key == pygame.K_z:
                    can_delete_bool = False
                    deleted = False
                    if len(space.bodies) > 5:
                        while not deleted:
                            for (body, notshape) in zip(space.bodies, space.shapes):
                                if not body == angleball.body and not notshape == angleball.shape:
                                    for wall in walls:
                                        if not wall[0] == body and not wall[1] == notshape:
                                            can_delete_bool = True
                            if can_delete_bool:
                                space.remove(body, notshape)
                                deleted = True
                if event.key == pygame.K_e:
                    angle += 0.105
                    space.remove(angleball.body, angleball.shape)
                    angleball = static_object((width - 31, 40), 25, pymunk.Circle, "anglecircle", space)
                if event.key == pygame.K_q:
                    angle -= 0.105
                    space.remove(angleball.body, angleball.shape)
                    angleball = static_object((width - 31, 40), 25, pymunk.Circle, "anglecircle", space)
                if event.key == pygame.K_ESCAPE or event.key == pygame.K_r:
                    reset_space()
                    create_walls(space, height)
            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                x,y = pos
                if x < 1030:
                    if event.button == 1:
                        obj = dynamic_object(pos, size, mass, inertia, shape, shape_name, space)
                    elif event.button == 3:
                        obj = static_object(pos, size, shape, shape_name, space)
                    elif event.button == 4:
                        angle += 0.105
                        space.remove(angleball.body, angleball.shape)
                        angleball = static_object((width - 31, 40), 25, pymunk.Circle, "anglecircle", space)
                    elif event.button == 5:
                        angle -= 0.105
                        space.remove(angleball.body, angleball.shape)
                        angleball = static_object((width - 31, 40), 25, pymunk.Circle, "anglecircle", space)

        screen.fill(bg_color)
        space.step(1/50)
        space.debug_draw(draw_options)
        pygame.display.update()
        clock.tick(fps)

if __name__ == "__main__":
    main()
